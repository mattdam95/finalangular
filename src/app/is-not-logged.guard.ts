import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UserDataService } from './login/user-data.service';

@Injectable({
  providedIn: 'root',
})
export class IsNotLoggedGuard implements CanLoad {
  constructor(
    private userDataService: UserDataService,
    private router: Router
  ) {}
  canLoad(
    route: Route,
    segments: UrlSegment[]
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (this.userDataService.isLogged) {
      return true;
    } else {
      return this.router.parseUrl('/');
    }
  }
}
