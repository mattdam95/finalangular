import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  SimpleChanges,
  ChangeDetectorRef,
} from '@angular/core';
import { Router } from '@angular/router';
import { ClothingDataService } from 'src/app/clothing/clothing-data.service';
import { UserDataService } from 'src/app/login/user-data.service';
import { ModalService } from '../modal/modal.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styles: [
    `
      a {
        cursor: pointer;
        .pt-3 {
          color: #247434;
          font-size: 14px;
          font-weight: 600;
        }
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavBarComponent {
  confirm: boolean = false;
  constructor(
    private userDataService: UserDataService,
    private clothingDataService: ClothingDataService,
    private modalService: ModalService,
    private router: Router,
    public cd: ChangeDetectorRef
  ) {}

  get isLogged() {
    return this.userDataService.isLogged;
  }

  logout() {
    this.router.navigateByUrl('/');
    this.userDataService.isLogged = false;
    this.userDataService.userLogged = '';
    localStorage.clear();
  }

  public openConfirmModal(typeOfModal: boolean, msj: string) {
    this.modalService
      .confirm(typeOfModal, 'Porfavor confirme:', msj)
      .then((confirmed) => {
        if (confirmed) {
          this.logout();
          this.cd.detectChanges();
        }
      });
  }

  //Obliga a guardar cambios si se esta editando
  public openWarningModal(typeOfModal: boolean, msj: string) {
    if (this.clothingDataService.editar) {
      this.modalService.confirm(typeOfModal, 'Atención', msj).then(() => {
        this.router.navigateByUrl('/clothing/cloth-form');
      });
    } else {
      this.router.navigateByUrl('/clothing/list-clothing');
    }
  }
}
