import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
} from '@angular/core';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styles: [
    `
      .message-box {
        width: 500px !important;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessageComponent implements OnInit {
  @Input() message: string | boolean = 'test';
  @Input() success: string | boolean = false;
  @Input() warning: string | boolean = false;
  @Input() hide: string | boolean = false;

  constructor() {}

  ngOnInit(): void {}
}
