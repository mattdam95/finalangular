import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { RouterModule } from '@angular/router';
import { MessageComponent } from './message/message.component';
import { ModalComponent } from './modal/modal.component';
import { ModalService } from './modal/modal.service';
@NgModule({
  declarations: [NavBarComponent, MessageComponent, ModalComponent],
  imports: [CommonModule, RouterModule],
  exports: [NavBarComponent, MessageComponent, ModalComponent],
})
export class SharedModule {}
