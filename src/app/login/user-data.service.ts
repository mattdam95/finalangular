import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserResponse } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root',
})
export class UserDataService {
  public isLogged: boolean = false;
  public userLogged!: string | null;
  public userCreated: boolean = false;

  public token: string | null = '';
  private url: string = 'http://localhost:3000/api/auth';

  constructor(private http: HttpClient) {}

  postUser(email: string, password: string) {
    return this.http.post<UserResponse>(`${this.url}/login`, {
      email: email,
      password: password,
    });
  }

  postNewUser(name: string, lastName: string, email: string, password: string) {
    return this.http.post<UserResponse>(`${this.url}/register`, {
      nombre: name,
      apellido: lastName,
      email: email,
      password: password,
    });
  }
}
