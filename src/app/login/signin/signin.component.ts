import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ErrorService } from 'src/app/error-page/error.service';
import { UserResponse } from 'src/app/interfaces/user.interface';
import { UserDataService } from '../user-data.service';

@Component({
  templateUrl: './signin.component.html',
})
export class SigninComponent implements OnInit {
  alerta: boolean = true;
  loginForm!: FormGroup;
  resultados!: UserResponse;
  error: boolean = false;
  validForm: boolean = false;

  //vars message
  userExists: boolean = false;
  userCreated: boolean = false;

  //vars NGclass
  emailValid: boolean = false;
  emailInvalid: boolean = false;
  invalidEmailFeedback: string = '';

  passValid: boolean = false;
  passInvalid: boolean = false;
  invalidPassFeedback: string = '';

  constructor(
    private userDataService: UserDataService,
    private errorService: ErrorService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: [
        '',
        Validators.compose([Validators.required, Validators.minLength(8)]),
      ],
    });
  }
  doSubmit() {
    const { email, password } = this.loginForm.value;

    //validacion
    if (!this.validForm) {
      return;
    }

    this.userDataService.postUser(email, password).subscribe(
      (res) => {
        this.resultados = res;
        this.userDataService.token = res.data.token;
        this.userDataService.isLogged = true;
        this.userDataService.userLogged = res.data.user.nombre;
        localStorage.tokenUser = res.data.token;
        localStorage.nameUser = res.data.user.nombre;
        this.router.navigateByUrl('/');
      },
      (err) => {
        if (err.status === 0) {
          this.errorService.typeOfError = '503';
          this.router.navigateByUrl('/error');
        }
        this.userExists = true;
        this.deleteMessage();
      }
    );
  }
  //permite mostrar un mensaje al crear un usuario
  get messageStateFromSiginup() {
    return this.userDataService.userCreated;
  }

  deleteMessage() {
    setTimeout(() => {
      this.userExists = false;
    }, 4000);
  }

  //revisa que input es y luego envia el msj de validacion corresp
  validations(event: any) {
    const input = event.srcElement.id;
    const { email, password } = this.loginForm.value;
    const { password: passwordCtrl, email: emailCtrl } =
      this.loginForm.controls;

    if (input === 'inputEmail') {
      if (email === '') {
        this.emailInvalid = true;
        this.invalidEmailFeedback = 'Completa este campo';
        return (this.validForm = false);
      }
      if (emailCtrl.invalid) {
        this.emailInvalid = true;
        this.invalidEmailFeedback = 'Email invalido';
        return (this.validForm = false);
      }
      this.emailInvalid = false;
    }
    if (input === 'inputPassword') {
      if (password === '') {
        this.passInvalid = true;
        this.invalidPassFeedback = 'Completa este campo';
        return (this.validForm = false);
      }
      if (passwordCtrl.invalid) {
        this.passInvalid = true;
        this.invalidPassFeedback = 'Min 8 caracteres';
        return (this.validForm = false);
      }
      this.passInvalid = false;
    }
    return (this.validForm = true);
  }
}
