import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ErrorService } from 'src/app/error-page/error.service';
import { UserResponse } from 'src/app/interfaces/user.interface';
import { UserDataService } from '../user-data.service';

@Component({
  templateUrl: './signup.component.html',
  styles: [
    `
      label {
        font-weight: bold;
        color: #247434 !important;
      }
    `,
  ],
})
export class SignupComponent implements OnInit {
  loginForm!: FormGroup;
  resultados!: UserResponse;
  error: number = 0;
  validForm: boolean = false;

  //vars VNGclass
  emailInvalid: boolean = false;
  invalidEmailFeedback: string = '';

  passInvalid: boolean = false;
  invalidPassFeedback: string = '';

  usernameInvalid: boolean = false;
  invalidUsernameFeedback: string = '';

  lastnameInvalid: boolean = false;
  invalidLastnameFeedback: string = '';

  constructor(
    private userDataService: UserDataService,
    private errorService: ErrorService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      username: ['', Validators.required],
      lastname: ['', Validators.required],
      password: [
        '',
        Validators.compose([Validators.required, Validators.minLength(8)]),
      ],
    });
  }
  doSubmit() {
    const { username, lastname, email, password } = this.loginForm.value;
    //validacion
    if (!this.validForm) {
      return;
    }

    this.userDataService
      .postNewUser(username, lastname, email, password)
      .subscribe(
        (res) => {
          this.resultados = res;
          this.router.navigateByUrl('/login/signin');
          this.userDataService.userCreated = true;
          this.deleteMessage();
        },
        (err) => {
          if (err.status === 0) {
            this.errorService.typeOfError = '503';
            this.router.navigateByUrl('/error');
          }
          this.error = err.status;
          this.deleteMessage();
        }
      );
  }

  //revisa que input es y luego envia el msj de validacion corresp
  validations(event: any) {
    const input = event.srcElement.id;
    const { email, username, lastname, password } = this.loginForm.value;
    const { email: emailCtrl, password: passwordCtrl } =
      this.loginForm.controls;

    if (input === 'inputEmail') {
      if (email === '') {
        this.emailInvalid = true;
        this.invalidEmailFeedback = 'Completa este campo';
        return (this.validForm = false);
      }
      if (emailCtrl.invalid) {
        this.emailInvalid = true;
        this.invalidEmailFeedback = 'Email invalido';
        return (this.validForm = false);
      }
      this.emailInvalid = false;
    }

    if (input === 'inputUsername') {
      if (username === '') {
        this.usernameInvalid = true;
        this.invalidUsernameFeedback = 'Completa este campo';
        return (this.validForm = false);
      }
      this.usernameInvalid = false;
    }

    if (input === 'inputLastname') {
      if (lastname === '') {
        this.lastnameInvalid = true;
        this.invalidLastnameFeedback = 'Completa este campo';
        return (this.validForm = false);
      }
      this.lastnameInvalid = false;
    }

    if (input === 'inputPassword') {
      if (password === '') {
        this.passInvalid = true;
        this.invalidPassFeedback = 'Completa este campo';
        return (this.validForm = false);
      }
      if (passwordCtrl.invalid) {
        this.passInvalid = true;
        this.invalidPassFeedback = 'Min 8 caracteres';
        return (this.validForm = false);
      }
      this.passInvalid = false;
    }

    return (this.validForm = true);
  }

  deleteMessage() {
    setTimeout(() => {
      this.userDataService.userCreated = false;
      this.error = 0;
    }, 4000);
  }
}
