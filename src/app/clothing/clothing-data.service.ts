import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  Cloth,
  ClothResponse,
  ClothsResponse,
} from '../interfaces/cloth.interface';

@Injectable({
  providedIn: 'root',
})
export class ClothingDataService {
  public token: string = '';
  private url: string = 'http://localhost:3000/api/clothing';

  public editar: boolean = false;
  public idEditar: string = '';

  public hideMessage: boolean = false;
  public success: boolean = false;
  public warning: boolean = false;
  public message: string = '';

  constructor(private http: HttpClient) {}
  getClothById(id: string) {
    return this.http.get<ClothResponse>(`${this.url}/${id}`);
  }

  getCloth() {
    return this.http.get<ClothsResponse>(this.url);
  }

  putClothById(
    id: string,
    tipo: string,
    cantidad: number,
    descripcion: string,
    precio: string
  ) {
    return this.http.put<Cloth>(`${this.url}/${id}`, {
      tipo,
      cantidad,
      descripcion,
      precio,
    });
  }

  deleteClothById(id: string) {
    return this.http.delete<Cloth>(`${this.url}/${id}`);
  }

  postCloth(
    tipo: string,
    cantidad: number,
    descripcion: string,
    precio: string
  ) {
    return this.http.post<Cloth>(this.url, {
      tipo,
      cantidad,
      descripcion,
      precio,
    });
  }
}
