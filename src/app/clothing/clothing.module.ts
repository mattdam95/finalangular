import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClothingRoutingModule } from './clothing-routing.module';
import { ListClothingComponent } from './list-clothing/list-clothing.component';
import { ClothComponent } from './cloth/cloth-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { ClothInfoComponent } from './cloth-info/cloth-info.component';

@NgModule({
  declarations: [ListClothingComponent, ClothComponent, ClothInfoComponent],
  imports: [
    CommonModule,
    ClothingRoutingModule,
    ReactiveFormsModule,
    SharedModule,
  ],
})
export class ClothingModule {}
