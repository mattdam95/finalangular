import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ErrorService } from 'src/app/error-page/error.service';
import { Cloth } from 'src/app/interfaces/cloth.interface';
import { ClothingDataService } from '../clothing-data.service';

@Component({
  templateUrl: './cloth-form.component.html',
  styles: [
    `
      textarea {
        resize: none;
      }
    `,
  ],
})
export class ClothComponent implements OnInit {
  clothForm!: FormGroup;
  resultados!: Cloth;
  validForm: boolean = false;
  subs!: Subscription;

  //NGclass
  tipoInvalid: boolean = false;
  invalidTipoFeedback: string = '';

  cantidadInvalid: boolean = false;
  invalidCantidadFeedback: string = '';

  descrInvalid: boolean = false;
  invalidDescrFeedback: string = '';

  precioInvalid: boolean = false;
  invalidPrecioFeedback: string = '';

  constructor(
    private clothDataService: ClothingDataService,
    private errorService: ErrorService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    /*Hace un get a la DB para saber los datos de la ropa a editar)
      Y asi colocarlos en los inputs por defecto */
    if (this.clothDataService.editar === true) {
      this.subs = this.clothDataService
        .getClothById(this.clothDataService.idEditar)
        .subscribe((res) => {
          this.resultados = res.data;
          const { tipo, cantidad, descripcion, precio } = this.resultados;
          this.createFormGroup(descripcion, tipo, precio, cantidad);
        });
    }
    this.createFormGroup('', '', 0, 0);
  }

  /*Si this.editar es falso agrega ropa a la DB,
    caso contrario edita uno ya existente*/
  doSubmit() {
    const { tipo, cantidad, descripcion, precio } = this.clothForm.value;

    //validacion
    if (this.clothDataService.editar) {
      this.validForm = true;
    }
    if (!this.validForm) {
      return;
    }

    if (this.clothDataService.editar === false) {
      this.clothDataService
        .postCloth(tipo, cantidad, descripcion, precio)
        .subscribe(
          (res) => {
            this.resultados = res;
            this.router.navigateByUrl('/clothing/list-clothing');
            this.messageToShow(
              true,
              false,
              true,
              'Ropa agregada correctamente'
            );
          },
          (err) => {
            if (err.status === 0) {
              this.errorService.typeOfError = '503';
              this.router.navigateByUrl('/error');
            }
          }
        );
    } else {
      this.clothDataService
        .putClothById(
          this.clothDataService.idEditar,
          tipo,
          cantidad,
          descripcion,
          precio
        )
        .subscribe(
          (res) => {
            this.resultados = res;
            this.clothDataService.editar = false;
            this.router.navigateByUrl('/clothing/list-clothing');
            this.messageToShow(true, false, true, 'Ropa editada correctamente');
          },
          (err) => {
            if (err.status === 0) {
              this.errorService.typeOfError = '503';
              this.router.navigateByUrl('/error');
            }
          }
        );
    }
  }

  validations(event?: any) {
    const input = event.srcElement.id;
    const { tipo, cantidad, descripcion, precio } = this.clothForm.value;
    const { cantidad: cantidadCtrl, precio: precioCtrl } =
      this.clothForm.controls;

    if (input === 'inputCantidad') {
      if (cantidad === null || cantidad === '') {
        this.cantidadInvalid = true;
        this.invalidCantidadFeedback = 'Completa este campo';
        return (this.validForm = false);
      }
      if (cantidadCtrl.invalid) {
        this.cantidadInvalid = true;
        this.invalidCantidadFeedback = 'Solo num positivos';
        return (this.validForm = false);
      }
      this.cantidadInvalid = false;
    }
    if (input === 'inputPrecio') {
      if (precio === null || precio === '') {
        this.precioInvalid = true;
        this.invalidPrecioFeedback = 'Completa este campo';
        return (this.validForm = false);
      }
      if (precioCtrl.invalid) {
        this.precioInvalid = true;
        this.invalidPrecioFeedback = 'Solo num positivos';
        return (this.validForm = false);
      }
      this.precioInvalid = false;
    }
    if (input === 'inputTipo') {
      if (tipo === null || tipo === '') {
        this.tipoInvalid = true;
        this.invalidTipoFeedback = 'Completa este campo';
        return (this.validForm = false);
      }
      this.tipoInvalid = false;
    }
    if (input === 'inputDescripcion') {
      if (descripcion === null || descripcion === '') {
        this.descrInvalid = true;
        this.invalidDescrFeedback = 'Completa este campo';
        return (this.validForm = false);
      }
      this.descrInvalid = false;
    }

    return (this.validForm = true);
  }

  createFormGroup(
    descripcion: string,
    tipo: string,
    precio: number,
    cantidad: number
  ) {
    this.clothForm = this.formBuilder.group({
      tipo: [tipo, Validators.required],
      cantidad: [
        cantidad,
        Validators.compose([Validators.required, Validators.min(0)]),
      ],
      descripcion: [descripcion, Validators.required],
      precio: [
        precio,
        Validators.compose([Validators.required, Validators.min(0)]),
      ],
    });
  }

  //Guarda el msj y tipo para despues mostrar en el listado de la ropa
  messageToShow(
    hide: boolean,
    warning: boolean,
    success: boolean,
    message: string
  ) {
    this.clothDataService.hideMessage = hide;
    this.clothDataService.success = success;
    this.clothDataService.warning = warning;
    this.clothDataService.message = message;
    this.deleteMessage();
  }

  deleteMessage() {
    setTimeout(() => {
      this.clothDataService.hideMessage = false;
    }, 4000);
  }

  ngOnDestroy() {
    if (this.subs !== undefined) {
      this.subs.unsubscribe();
    }
  }
}
