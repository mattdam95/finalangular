import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cloth } from 'src/app/interfaces/cloth.interface';
import { ClothingDataService } from '../clothing-data.service';

@Component({
  templateUrl: './cloth-info.component.html',
  styles: [
    `
      ul {
        list-style-type: none;
      }
      span {
        font-weight: bold;
        color: #247434;
      }
    `,
  ],
})
export class ClothInfoComponent implements OnInit {
  cloth!: Cloth;
  resLoaded: boolean = false;
  constructor(
    private clothingDataService: ClothingDataService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.clothingDataService
      .getClothById(this.clothingDataService.idEditar)
      .subscribe((res) => {
        this.cloth = res.data;
        this.resLoaded = true;
      });
  }

  goToClothList() {
    this.router.navigate(['/clothing/list-clothing']);
  }
}
