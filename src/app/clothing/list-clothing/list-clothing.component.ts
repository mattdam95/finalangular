import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Cloth } from 'src/app/interfaces/cloth.interface';
import { ModalService } from 'src/app/shared/modal/modal.service';
import { ClothingDataService } from '../clothing-data.service';

@Component({
  templateUrl: './list-clothing.component.html',
  styleUrls: ['./list-clothing.component.scss'],
})
export class ListClothingComponent implements OnInit {
  respuesta: Cloth[] = [];
  id: string = '';
  borrar: boolean = false;
  subs!: Subscription;

  //Variables msj
  message: string = '';
  success!: boolean;
  warning!: boolean;
  hide: boolean = false;

  constructor(
    private clothingDataService: ClothingDataService,
    private modalService: ModalService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getCloth();
  }

  //gets
  get resultados() {
    return this.respuesta;
  }
  get messageState() {
    return [
      this.clothingDataService.hideMessage,
      this.clothingDataService.success,
      this.clothingDataService.warning,
      this.clothingDataService.message,
    ];
  }
  //--------------------------------------------------------

  deleteCloth(event: any) {
    this.id = event.target.parentElement.parentElement.firstChild.outerText;
    this.clothingDataService.deleteClothById(this.id).subscribe((res) => {
      this.getCloth();
      this.typeOfMessage(true, false, true, 'Ropa eliminada');
    });
  }
  //Redirecciona a una pagina para editar y manda los datos de la ropa
  editCloth(event: any) {
    this.id = event.target.parentElement.parentElement.firstChild.outerText;
    this.clothingDataService.editar = true;
    this.clothingDataService.idEditar = this.id;
    this.router.navigate(['/clothing/cloth-form']);
  }

  goToClothInfo(editar: any) {
    this.id = editar.target.parentElement.parentElement.firstChild.outerText;
    this.clothingDataService.idEditar = this.id;
  }

  //define si el tipo de mensaje a mostrar es un warning o succes
  typeOfMessage(
    hide: boolean,
    warning: boolean,
    success: boolean,
    message: string
  ) {
    this.clothingDataService.hideMessage = hide;
    this.clothingDataService.success = success;
    this.clothingDataService.warning = warning;
    this.clothingDataService.message = message;
    this.deleteMessage();
  }

  deleteMessage() {
    setTimeout(() => {
      this.clothingDataService.hideMessage = false;
    }, 4000);
  }

  getCloth() {
    this.subs = this.clothingDataService.getCloth().subscribe((res) => {
      this.respuesta = res.data;
    });
  }

  openConfirmModal(event: any, typeOfModal: boolean, msj: string): any {
    this.modalService
      .confirm(typeOfModal, 'Porfavor confirme:', msj)
      .then((confirm) => {
        if (confirm) {
          this.deleteCloth(event);
        }
      });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
