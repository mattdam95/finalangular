import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorPageComponent } from '../error-page/error-page.component';
import { ClothInfoComponent } from './cloth-info/cloth-info.component';
import { ClothComponent } from './cloth/cloth-form.component';
import { ListClothingComponent } from './list-clothing/list-clothing.component';

const routes: Routes = [
  { path: 'cloth-form', component: ClothComponent },
  { path: 'list-clothing', component: ListClothingComponent },
  { path: 'cloth-info/:id', component: ClothInfoComponent },
  { path: '**', component: ErrorPageComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClothingRoutingModule {}
