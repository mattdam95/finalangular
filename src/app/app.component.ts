import { Component, OnInit } from '@angular/core';
import { UserDataService } from './login/user-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private userDataService: UserDataService) {}
  ngOnInit() {
    if (localStorage.tokenUser !== undefined) {
      // Código cuando Storage es compatible
      let token = localStorage.getItem('tokenUser'),
        name = localStorage.getItem('nameUser');
      this.userDataService.token = token;
      this.userDataService.userLogged = name;
      this.userDataService.isLogged = true;
    }
  }
}
