import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../login/user-data.service';

@Component({
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  constructor(private userDataService: UserDataService) {}
  ngOnInit(): void {}

  get isLogged() {
    return this.userDataService.isLogged;
  }

  get name() {
    return this.userDataService.userLogged;
  }
}
