export interface ClothsResponse {
  code: string;
  message: null;
  success: boolean;
  data: Cloth[];
}

export interface ClothResponse {
  code: string;
  message: null;
  success: boolean;
  data: Cloth;
}

export interface Cloth {
  _id: string;
  tipo: string;
  cantidad: number;
  descripcion: string;
  precio: number;
  createdAt: Date;
  updatedAt: Date;
  __v: number;
}
