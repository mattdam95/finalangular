export interface UserResponse {
  code: string;
  message: null;
  success: boolean;
  data: Data;
}

export interface Data {
  user: User;
  token: string;
}

export interface User {
  rol: string;
  _id: string;
  nombre: string;
  apellido: string;
  email: string;
}
