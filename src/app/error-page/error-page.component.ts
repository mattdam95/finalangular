import { Component, OnInit } from '@angular/core';
import { ErrorService } from './error.service';

@Component({
  templateUrl: './error-page.component.html',
  styles: [
    `
      img {
        width: 200px;
        heigth: 200px;
      }
      p {
        font-size: 24px;
      }
      span {
        font-weight: bolder;
        font-size: 40px;
      }
      .d-flex {
        gap: 10px;
      }
    `,
  ],
})
export class ErrorPageComponent implements OnInit {
  constructor(private errorService: ErrorService) {}

  ngOnInit(): void {}

  get errorType(): string {
    return this.errorService.typeOfError;
  }
}
