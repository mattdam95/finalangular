import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ErrorService {
  public error: boolean = false;
  public typeOfError: string = '404';
  constructor() {}
}
