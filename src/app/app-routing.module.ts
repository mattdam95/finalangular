import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorPageComponent } from './error-page/error-page.component';
import { HomeComponent } from './home/home.component';
import { IsLoggedGuard } from './is-logged.guard';
import { IsNotLoggedGuard } from './is-not-logged.guard';

const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'login',
    canLoad: [IsLoggedGuard],
    loadChildren: () =>
      import('./login/login.module').then((m) => m.LoginModule),
  },
  {
    path: 'clothing',
    canLoad: [IsNotLoggedGuard],
    loadChildren: () =>
      import('./clothing/clothing.module').then((m) => m.ClothingModule),
  },
  { path: '**', component: ErrorPageComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
