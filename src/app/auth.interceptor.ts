import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserDataService } from './login/user-data.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private userDataService: UserDataService) {}

  intercept(
    req: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    if (req.url.includes('login')) {
      return next.handle(req);
    }
    const authHeader = `Bearer ${this.userDataService.token}`;
    const authReq = req.clone({
      headers: req.headers.set('Authorization', authHeader),
    });
    return next.handle(authReq);
  }
}
